
import CourseCard from '../components/CourseCard.js';
import {Fragment, useEffect, useState} from 'react';
import {Container} from 'react-bootstrap';

export default function Courses () {

	// const courses = coursesData.map( course => {

	// 	return(
	// 		<CourseCard key = {course.id} courseProp = {/*course*/}/>
	// 		)
	// })

	// Let's put an empty array to signify courses' initial state.
	const [courses, setCourses] = useState([]);



	useEffect(() => {
		// Fetch all courses in API database.
		fetch(`${process.env.REACT_APP_API_URL}/course/allActive`)
		.then(result => result.json())
		.then(data => {
			console.log(data);
			// To change the value of our courses, we have to use setCourses.
			setCourses(data.map(course => {
				return(
					<CourseCard key = {course._id} courseProp = {course}/>
					);
				})
			);
		});

	},[]);

	return(
		<Fragment>
			<Container>
			<h1 className = "text-center mt-3">Courses</h1>
			{courses}
			</Container>
		</Fragment>// We need the <Fragment> tags since {courses} will return multiple components rather than just one.
		)
};