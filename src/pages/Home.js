import {Fragment} from 'react';
import {Container} from 'react-bootstrap';

import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';

export default function Home () {

	return(
		<Fragment>
		<Container>
			<Banner/>
			<Highlights/>
		</Container>
		</Fragment>
		)
};