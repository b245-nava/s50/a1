import React from 'react';

// Create a Context Object

	// As the name states, a context object is an object data type that can be used to store information that can be shared to other components within the app.
	// The context object is a different approach to passing information between components and allows easier access by avoiding the use of prop drilling.

const UserContext = React.createContext();

// The .Provider property of createContext() allows other components to consume/use the context object and supply the necessary information.
export const UserProvider = UserContext.Provider;

export default UserContext;