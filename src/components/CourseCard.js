import {Card, Button} from 'react-bootstrap';
import {Row, Col} from 'react-bootstrap';
import {useContext, useState, useEffect} from 'react';
import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';

export default function CourseCard ({courseProp}) {

	const {_id, name, description, price} = courseProp;
	// The "course" in the CourseCard component is called a "prop," which is shorthand for "property."

	// The curly braces are used to signify that we are providing information using expressions.

	// Use the state hook for this component to be able to store its state.
	// States are used to keep track of information related to individual components.
		/*
			Syntax:
				const [getter, setter] = useState(initialGetterValue);

				- getter will be our variable, while setter will be a function that will change the value of our getter
				- initialGetterValue will contain the getter's initial value
		*/

	// Note that useStates are only accessible internally; used to maintain a local component.
	// useEffect is used to perform an operation after a render that may result in changes

	const [enrollees, setEnrollees] = useState(0);
	const [seats, setSeats] = useState(30);
	// Add a new state that will declare whether the button is disabled or not
	const [isDisabled, setIsDisabled] = useState(false);


	// Initial value of enrollees state

		// console.log(enrollees); // returns 0

	// If you want to change/reassign the value of the state:

		// setEnrollees(1);
		// console.log(enrollees);

	const {user} = useContext(UserContext);

	function enroll () {
		
		if(enrollees < 29 && seats > 1){
			setEnrollees(enrollees+1);
			setSeats(seats-1);
		} else {
			alert('Congratulations! You are the final enrollee.');
			setEnrollees(enrollees+1);
			setSeats(seats-1);
		}
	};

	// Define a useEffect hook to have the CourseCard component perform a certain task
	// This will run automatically.
		/*
			Syntax: 
				useEffect(sideEffect/function, [dependencies]);

				- sideEffect/function: runs on the first load and will reload depending on the dependency array
		*/

	useEffect(()=> {
		if(seats === 0){
			setIsDisabled(true);
		}
	}, [seats]);


	return (
		<Row className = "mt-4 mb-4">
			<Col>
				<Card>
			      <Card.Body>
			        <Card.Title>{name}:</Card.Title>
		        	<Card.Subtitle>Description:</Card.Subtitle>
	        		<Card.Text>{description}</Card.Text>
	        		<Card.Subtitle>Price:</Card.Subtitle>
	        		<Card.Text>PhP {price}</Card.Text>

	        		<Card.Subtitle>Enrollees:</Card.Subtitle>
	        		<Card.Text>{enrollees}</Card.Text>
	        		
	        		<Card.Subtitle>Available Seats:</Card.Subtitle>
	        		<Card.Text>{seats}</Card.Text>
			        
	        		{
	        			user?
	        			<Button as = {Link} to = {`/course/${_id}`} disabled = {isDisabled}>Course Details</Button>
	        			:
	        			<Button as = {Link} to = "/login">Log In</Button>
	        		}

			      </Card.Body>
			    </Card>
		    </Col>
		</Row>
		)
};
// To invoke multiple functions in onClick, use {function1 ; function2; ...}