// Import Bootstrap Classes here
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {useContext} from 'react';
import {Fragment} from 'react';
import UserContext from '../UserContext.js';

import {Link, NavLink} from 'react-router-dom';

/*import {Container, Nav, Navbar} from 'react-bootstrap'*/

export default function AppNavBar(){
	
	// To retrieve an item from Local Storage, you can use .getItem("propertyToBeRetrieved").
	// console.log(localStorage.getItem('email'));

	// Let's create a new state for the user.
	// const [user, setUser] = useState(localStorage.getItem('email'));

	// Out of the three possible variables, we use "user" to determine the user's state.
	const {user} = useContext(UserContext);


	return (
		<Navbar bg="light" expand="lg">
		      <Container>
		      	{/*Using the "as" keyword, Navbar.Brand inherited the properties of {Link}. Meanwhile, "to" linked Navbar.Brand to the declared url endpoint.*/}
		        <Navbar.Brand as = {Link} to = "/">Zuitt</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		        	{/*me refers to "margin end" auto, while ms refers to "margin start."*/}
		        	<Nav className="ms-auto">
			            <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
			            <Nav.Link as = {NavLink} to = "/courses">Courses</Nav.Link>

			            {/*Conditional Rendering*/}
			            {
			            	user ?
			            	<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
			            	:
			            	<Fragment>
			            		<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
			            		<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
			            	</Fragment>
			            }
			            
		          	</Nav>
		        </Navbar.Collapse>
	      	</Container>
	    </Navbar>
		)

};