// To create a React app, type:
	// npx create-react-app project-name

// npm : node :: npx : javascript

// To install React-Bootstrap and Bootstrap:
	// npm install react-bootstrap bootstrap

// To run our react application:
	// npm start in the project-name folder

/* Files to remove:
	From folder src:
	- App.test.js
	- index.css
	- reportWebVitals.js 
	- logo.svg 

	Remove their importations from App.js and index.js as well.*/


// The 'import' statement allows us to use the code/export modules from other files, similar to how we use the 'require' function in Node.js.


// React JS applies the concepts of Rendering and Mounting in order to display and create components.
	// Mounting: The display of components in the app/website; also known as when React.js "renders" or "displays" the component in the initial DOM based on the instructions.
	// Render: The calling/invoking of functions or a component that returns a set of instructions.

// Using event.target.value will capture the value inputted by the user in the input area.

// Regarding the [dependencies] in useEffect:
	// 1. Single dependency [dependency]
		// The sideEffect or the function will be triggered on the initial load and when a change in the dependency is noted..
	// 2. Empty dependency []
		// The sideEffect will run only during the initial page load.
	// 3. Multiple dependency [dep1, dep2, ...]
		// The sideEffect will run during the initial load and whenever the states of any of the dependencies change.

// [Section] react-router-dom
	// To install:
		// npm i react-router-dom
	// For us to be able to use the module/library across all of our pages, we have to contain them with BrowserRouter.

	// Using Routes, we contain all of the routes in our react-app.
	// Route refers to a specific route where we declare the url and also the component or page to be mounted.

// [Section] NavLink and Link
	// We use Link to go from one page to another
	// We use NavLink to change our page using our NavBar

// "localStorage.setItem" allows us to manipulate the browser's localStorage property to store information indefinitely.

// useState() is only accessible within the code in which it is defined. On the other hand, useContext() has a global reach.

// [Section] Environment Variable
	// Environment variables are important in hiding sensitive pieces of information like the backend API, which can be exploited if added directly into our code.
	// You set these in an .env.local file, and whenever your set the variables, you will need to restart the server.